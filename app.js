var restify = require('restify'),
	mongoose = require('mongoose'),
	fs = require("fs");

mongoose.connect('mongodb://testuser:testuser@127.0.0.1/heroes');
var models_path = './models';
fs.readdirSync(models_path).forEach(function (file) {
    require(models_path+'/'+file)
});

function respond(req, res, next) {
  res.send('hello ' + req.query.abc);
  next();
}

var gcm = require('./controllers/gcm');
var tracking = require('./controllers/track');
var user = require('./controllers/user');

var server = restify.createServer();

server.use(restify.queryParser());

server.get('/initialnotify',gcm.initialNotify);
server.get('/gcmregistration',gcm.gcmRegistration);
server.get('/acknowledge',tracking.acknowledge);
server.get('/updateposition',tracking.getallpositions);
server.get('/getallpositions',tracking.getallpositions);
server.get('/searchbyname',user.searchByName);
server.post('/saveUserInfo',user.saveUserInfo);
server.get('/getUserInfo',user.getUserInfo);

server.get('/hello/:name', respond);

server.listen(8080, function() {
  console.log('%s listening at %s', server.name, server.url);
});