var gcm = require('node-gcm');
var message = new gcm.Message();
var mongoose = require('mongoose');
var UserModel = mongoose.model('UserModel');
var TrackModel = mongoose.model('TrackModel');

function getAllRegistrationIds(){
	var gcmregids = [];
	UserModel.find('gcm_reg_id',function(err, data){
		if(data){
			data.forEach(function(row){
				gcmregids.push(row.gcm_reg_id);
			});
		}
		if(err){
			gcmregids = null;
		}
	});
	return gcmregids
}

function getEmergencyContacts(user){
	var gcmregids = [];
	UserModel.find({email:user},'emergency_contacts',function(err, data){
		if(data){
			var emergencyContacts = data.emergency_contacts;
			emergencyContacts.forEach(function(row){
					gcmregids.push(row.gcm_reg_id);
			});
		}
		if(err){
			gcmregids = null;
		}
	});
	return gcmregids
}

function sendMessage(alerttype,user,latitude,longitude,trackId){
	var registrationIds = [];
	var serverKey = "AIzaSyBNkJRZwjs9ZQS7pgCDiBBdZx1P3vNOFlg";
	var sender = new gcm.Sender(serverKey);
	if(alerttype == 'sos'){
		registrationIds = getAllRegistrationIds();

		message.addData('message',"Location:("+latitude+","+longitude+")-Track Id:"+trackId);
		message.addData('title','HELP ME');
		message.addData('msgcnt','3'); // Shows up in the notification in the status bar
		message.addData('soundname','beep.wav'); //Sound to play upon notification receipt - put in the www folder in app
		//message.delayWhileIdle = true; //Default is false
		message.timeToLive = 3000;// Duration in seconds to hold in GCM and retry before timing out. Default 4 weeks (2,419,200 seconds) if not specified.
 
	}
	else if(alerttype == 'trackMe'){
		registrationIds = getEmergencyContacts(user);

		message.addData('message',"Location:"+latitude+","+longitude+"-Track Id:"+trackId);
		message.addData('title','TRACK ME');
		message.addData('msgcnt','3'); // Shows up in the notification in the status bar
		message.addData('soundname','beep.wav'); //Sound to play upon notification receipt - put in the www folder in app
		//message.delayWhileIdle = true; //Default is false
		message.timeToLive = 3000;// Duration in seconds to hold in GCM and retry before timing out. Default 4 weeks (2,419,200 seconds) if not specified.

	}
 
	sender.send(message, registrationIds, 4, function (result) {
	    if(result){
	    	console.log('Result',result);
	    }
	    return result;
	});	
}


/**
*	SAVE USER'S GCM Registration ID
**/
exports.gcmRegistration = function(req,res){
	var user = req.param('user');
	var gcm_reg_id = req.param('regid');
	var query = {email: user};
	var update = { gcm_reg_id: gcm_reg_id };
	console.log("user",user);
	console.log("regid",gcm_reg_id);
	if(user != null & gcm_reg_id != null){
		UserModel.findOneAndUpdate(query, update,function(err,user){
			if (err)
				res.send('not saved');
		});		
		res.send('saved');
	}
	else{
		res.send('not saved');
	}	
};



/**
*	SAVE THE SOS SIGNAL IN TRACK
**/
exports.initialNotify = function(req,res){
	console.log("Inside Initial notify !!");
	var latitude = req.param('latitude');
	var longitude = req.param('longitude');
	var user = req.param('user');
	var type = req.param('type');
	var result = null;

	var tracking = new TrackModel({victim: {username: user,longitude: longitude,latitude: latitude}});
	tracking.save(function (err,track) {
  		if(err) {
  			console.log('Save Error');
  			result = err;
  		}
  		if(track){
  			console.log('Save Success');
  			result = sendMessage(type,user,latitude,longitude,track._id);
  		}
	});

	res.send(result);

};