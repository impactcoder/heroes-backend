var mongoose = require('mongoose'),
	UserModel = mongoose.model('UserModel');

/**
*	GET USER'S LISTING
**/

exports.index = function(req,res){
	UserModel.find(function(err, users){
		console.log(users);
	});
	res.send('user listing');
	
};

exports.searchByName = function(req,res){
	var name = "^"+req.param('name')+".*";
	var query = {'name' : {'$regex' : name,'$options' : 'i'}};
	var projection = {'name':1,'_id':0};
	var usernames = [];
	UserModel.find(query,projection,function(err,users){
		// Log the error for debugging purpose
		if(err){
			console.log("FILE user.js METHOD searchByName ERROR ",err);
		} else if(users != null || users != undefined){
			// forEach is not used to avoid asynch operation
			for(i=0;i<users.length;i++){
				usernames.push(users[i].name);
			}
		}
		console.log(usernames);
		return usernames;
	});
};

/**
*	GET USER'S PROFILE INFO
**/
exports.getUserInfo = function(req, res, next){
	var uname = req.query.uname;
	console.log('username'+uname);
	UserModel.findOne({'name': uname}, function(err, user){
		// Log the error for debugging purpose
		if(err){
			console.log("FILE user.js METHOD getUserInfo ERROR ",err);
		} 
		console.log(user);
		res.send(user);
	});
};

/**
*	UPDATE USER'S PROFILE INFO
**/
exports.saveUserInfo = function(req, res){
	var uname = req.param('uname');
	var name = req.param('name');
	var email = req.param('email');
	var mobile = req.param('mobile');
	console.log("user name = "+uname);
	var isUpdated = false;
	UserModel.findOne({'name': uname}, function(err, user){
		// Log the error for debugging purpose
		if(err){
			console.log("FILE user.js METHOD saveUserInfo ERROR ",err);
		} 
		else if(user != null || user != undefined){
			user.name = name;
			user.email = email;
			user.mobile = mobile;
			user.save(function(err){
				if(err){
					console.log("FILE user.js METHOD saveUserInfo ERROR" + err);
					res.send(isUpdated);
				}
			});
		}
		console.log("user = %j",user);
		res.send(!isUpdated);
	});
};
